package Main;

import java.util.HashSet;
import java.util.Set;

public class SetOperations {

    public static boolean isSubset(Set<?> es, Set<?> ts) {
        if (es.containsAll(ts)) return true;
        return false;
    }

    public static<T> Set<T> union(Set<T> es, Set<T> ts) {
        Set<T> alls = new HashSet<>();
        alls.addAll(es);
        alls.addAll(ts);
        return alls;
    }

    public static<T> Set<T> intersection(Set<T> es, Set<T> ts) {
        Set<T> alls = new HashSet<>();
        alls.addAll(es);
        alls.retainAll(ts);
        return alls;
    }

    public static<T> Set<T> difference(Set<T> es, Set<T> ts) {
        Set<T> alls = new HashSet<>();
        alls.addAll(es);
        alls.removeAll(ts);
        return alls;
    }

    public static<T> Set<T> symDifference(Set<T> es, Set<T> ts) {
        Set<T> alls = new HashSet<>();
        Set<T> alls1 = new HashSet<>();
        alls.addAll(es);
        alls.removeAll(ts);
        alls1.addAll(ts);
        alls1.removeAll(es);
        alls.addAll(alls1);
        return alls;
    }
}
